using System;

namespace ZAD6
{
    class Entry
    {
        static void Main(string[] args)
        {
            Director director = new Director(new NotificationBuilder());
            NotificationManager manager = new NotificationManager();
            manager.Display(director.LogAlert("Fran"));
        }
    }
}
