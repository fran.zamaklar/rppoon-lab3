using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD6
{
    class Director
    {
        IBuilder builder = new NotificationBuilder();
        public Director(IBuilder builder) 
        {
            this.builder = builder;
        }
        public void SetBuilder(IBuilder builder)
        {
            this.builder = builder;
        }

        public ConsoleNotification LogInfo(string author)
        {
            return builder.SetAuthor(author).SetTitle("Notification").SetText("Information").SetLevel(Category.INFO).SetTime(DateTime.Now).SetColor(ConsoleColor.White).Build();
        }
        public ConsoleNotification LogAlert(string author)
        {
            return builder.SetAuthor(author).SetTitle("Notification").SetText("Information").SetLevel(Category.ALERT).SetTime(DateTime.Now).SetColor(ConsoleColor.Red).Build();
        }
        public ConsoleNotification LogError(string author)
        {
            return builder.SetAuthor(author).SetTitle("Notification").SetText("Information").SetLevel(Category.ERROR).SetTime(DateTime.Now).SetColor(ConsoleColor.Black).Build();
        }
    }
}
