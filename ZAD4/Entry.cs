using System;

namespace ZAD4
{
    class Entry
    {
        static void Main(string[] args)
        {
            ConsoleNotification notification = new ConsoleNotification("Fran", "Instagram", "Novi pratitelji", DateTime.Now, Category.ERROR, ConsoleColor.Cyan);
            NotificationManager notify = new NotificationManager();
            notify.Display(notification);
        }
    }
}
