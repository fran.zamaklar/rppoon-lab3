using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD7
{
    interface IPrototype
    {
        IPrototype Clone();
    }
}
