using System;

namespace ZAD7
{
    class Entry
    {
        static void Main(string[] args)
        {
            ConsoleNotification notification = new ConsoleNotification("Fran", "Instagram", "Novi pratitelji", DateTime.Now, Category.ERROR, ConsoleColor.Cyan);
            ConsoleNotification notificationClone = (ConsoleNotification)notification.Clone();
            NotificationManager manager = new NotificationManager();
            notificationClone.setColor(ConsoleColor.DarkGray);
            manager.Display(notification);
            manager.Display(notificationClone);
            //Nema razlike između dubokog i plitkog kopiranja jer nemamo sto duboko kopirati. Imamo samo osnovne tipove podataka, a ne slozene tipove podataka (klase).
        }
    }
}
