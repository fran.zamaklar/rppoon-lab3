using System;

namespace ZAD5
{
    class Entry
    {
        static void Main(string[] args)
        {
            NotificationBuilder builder = new NotificationBuilder();
            builder.SetAuthor("Fran");
            builder.SetTitle("Message");
            builder.SetText("Bok!");
            builder.SetTime(DateTime.Now);
            builder.SetLevel(Category.INFO);
            builder.SetColor(ConsoleColor.Yellow);
            NotificationManager manager = new NotificationManager();
            manager.Display(builder.Build());
        }
    }
}
