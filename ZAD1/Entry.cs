using System;

namespace ZAD1
{
    class Entry
    {
        static void Main(string[] args)
        {
            Dataset dataset = new Dataset(@"C:\Users\Lenovo\Documents\Filename.csv");
            IPrototype copiedData = dataset.Clone();
            dataset.ToString();
            copiedData.ToString();
            //Nije potrebno duboko kopirati zato sto taj tip podataka ne zahtjeva duboko kopiranje
        }
    }
}
