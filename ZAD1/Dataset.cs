using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD1
{
    class Dataset : IPrototype
    {
        private List<List<string>> data; //list of lists of strings
        public Dataset()
        {
            this.data = new List<List<string>>();
        }
        public Dataset(string filePath) : this()
        {
            this.LoadDataFromCSV(filePath);
        }
        public void LoadDataFromCSV(string filePath)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    List<string> row = new List<string>();
                    string[] items = line.Split(',');
                    foreach (string item in items)
                    {
                        row.Add(item);
                    }
                    this.data.Add(row);
                }
            }
        }
        public IList<List<string>> GetData()
        {
            return
           new System.Collections.ObjectModel.ReadOnlyCollection<List<string>>(data);
        }
        public void ClearData()
        {
            for(int i = 0; i < data.Count; i++)
            {
               this.data[i].Clear();
            }

            this.data.Clear();
        }

        public IPrototype Clone()
        {
            Dataset deepCopy = (Dataset)this.MemberwiseClone();
            List<List<string>> dataCopy = new List<List<string>>();

            for (int i = 0; i < this.data.Count; i++)
            {
                List<string> strings = new List<string>();

                for (int j = 0; j < this.data[i].Count; j++)
                {
                    strings.Add(data[i][j]);
                }
                dataCopy.Add(strings);
            }
            deepCopy.data = dataCopy;
            return deepCopy;
        }

        public void ToString()
        {
            for(int i = 0; i < this.data.Count; i++)
            {
                for(int j = 0; i < this.data[i].Count; j++)
                {
                    Console.Write(data[i][j]);
                }
                Console.WriteLine();
            }
        }
    }
}
