using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD2
{
    class MatrixGenerator
    {
        public static MatrixGenerator instance;
        private RandomGenerator random;

        private MatrixGenerator()
        {
            random = RandomGenerator.GetInstance();
        }

        public static MatrixGenerator GetInstance()
        {
            if (instance == null)
            {
                instance = new MatrixGenerator();
            }
            return instance;
        }

        public float[][] CreateMatrix(int width, int height)
        {
            float[][] matrix = new float[height][];
            for (int i = 0; i < matrix.Length; i++)
            {
                matrix[i] = new float[width];
            }

            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    matrix[i][j] = (float)random.NextDouble();
                }
            }
            return matrix;
        }

        public static void ToString(float[][] matrix)
        {
            for(int i = 0; i < matrix.Length; i++)
            {
                for(int j = 0; j < matrix[i].Length; j++)
                {
                    Console.Write(matrix[i][j] + "\t");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }
}
