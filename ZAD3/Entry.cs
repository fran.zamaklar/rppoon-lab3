using System;

namespace ZAD3
{
    class Entry
    {
        static void Main(string[] args)
        {
            Logger logger = Logger.GetInstance();
            logger.SetFilePath(@"C:\Users\Lenovo\Documents\Example.txt");
            Logger.GetInstance().Log("Beginning");
            Logger.GetInstance().Log("Ending");
            //Ukoliko je singleton promijenjen u kodu, on će se trajno promijeniti tako da će u mainu kada promijenimo file path, tekst će se upisivati u tu datoteku koju smo dali kao parametar.
        }
    }
}
