using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace ZAD3
{
    class Logger
    {
        public static Logger instance;
        string filePath;

        private Logger()
        {
            filePath = @"C:\File.txt";
        }
        public static Logger GetInstance()
        {
            if(instance == null)
            {
                instance = new Logger();
            }
            return instance;
        }
        public void SetFilePath(string filepath)
        {
            filePath = filepath;
        }
        public void Log(string message)
        {
            StreamWriter file = new StreamWriter(filePath, true);
            file.Write(message);
            file.Close();
        }


    }
}
